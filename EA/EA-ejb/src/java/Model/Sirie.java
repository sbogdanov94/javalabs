/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author 1
 */
@Entity
@Table(name = "sirie")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Sirie.findAll", query = "SELECT s FROM Sirie s"),
    @NamedQuery(name = "Sirie.findBySiriename", query = "SELECT s FROM Sirie s WHERE s.siriename = :siriename"),
    @NamedQuery(name = "Sirie.findByMoney", query = "SELECT s FROM Sirie s WHERE s.money = :money"),
    @NamedQuery(name = "Sirie.findById", query = "SELECT s FROM Sirie s WHERE s.id = :id")})
public class Sirie implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "Sirie_name")
    private String siriename;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Money")
    private int money;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    public Sirie() {
    }

    public Sirie(Integer id) {
        this.id = id;
    }

    public Sirie(Integer id, String siriename, int money) {
        this.id = id;
        this.siriename = siriename;
        this.money = money;
    }

    public String getSiriename() {
        return siriename;
    }

    public void setSiriename(String siriename) {
        this.siriename = siriename;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sirie)) {
            return false;
        }
        Sirie other = (Sirie) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Model.Sirie[ id=" + id + " ]";
    }
    
}
