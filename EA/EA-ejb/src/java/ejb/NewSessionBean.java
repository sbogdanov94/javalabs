/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ejb;

import Model.Airport;
import Model.Plane;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author 1
 */
@Stateless
@LocalBean
public class NewSessionBean implements NSInterface {

    @PersistenceContext(unitName = "EA-ejbPU")
    private EntityManager em;
    
    @Override
    public void AddPlane(Plane plane) {
        em.persist(plane);
    }

    @Override
    public void AddAirport(Airport airport) {
        em.persist(airport);
    }

    @Override
    public List<Plane> ShowPlanes() {
        Query query = em.createQuery("SELECT p FROM Plane p");
        return query.getResultList();
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")

    @Override
    public Airport SelectAirport(String air) {
        Query query = em.createQuery("SELECT p from Airport p WHERE p.name = '" + air + "'");
        return (Airport) query.getSingleResult();
    
    }
}
