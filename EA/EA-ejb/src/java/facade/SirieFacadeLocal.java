/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package facade;

import Model.Sirie;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author 1
 */
@Local
public interface SirieFacadeLocal {

    void create(Sirie sirie);

    void edit(Sirie sirie);

    void remove(Sirie sirie);

    Sirie find(Object id);

    List<Sirie> findAll();

    List<Sirie> findRange(int[] range);

    int count();
    
    public void createExample1(Sirie sirie);
    public void createExample5(Sirie sirie);
}
