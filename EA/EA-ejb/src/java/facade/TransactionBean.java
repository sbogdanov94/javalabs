/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package facade;

import Model.Plane;
import Model.Sirie;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author 1
 */
@Stateless
public class TransactionBean implements TransactionBeanLocal {

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    
    @EJB
    private SirieFacadeLocal sfl;
    
    
    @EJB
    private PlaneFacadeLocal pfl;
    
    
    
    

    @PersistenceContext(unitName = "EA-ejbPU")
    EntityManager emDB1;
    
    @PersistenceContext(unitName = "EA-ejbPU2")
    EntityManager emdb2;
    
    
    @Override
    public void createNewEntityEX1(Plane plane, Sirie sirie) {
        emDB1.persist(plane);
        emDB1.flush();
        emdb2.persist(sirie);
        emdb2.flush();
    }
    
    @Override
    public void createNewEntityEX2(Plane plane, Sirie sirie) {
        sfl.create(sirie);
        pfl.CreateExample3(plane);
    }
    
    @Override
    public void createNewEntityEX3(Plane plane, Sirie sirie) {
       sfl.create(sirie);
       pfl.CreanteNewEntituEx2();
    }
    
    
    @Override
    public void createNewEntityEX4(Plane plane, Sirie sirie) {
       pfl.CreateExample4(plane);
       sfl.createExample1(sirie);
    }
    
    @Override
    public void createNewEntityEX5(Plane plane, Sirie sirie){
       sfl.createExample5(sirie);
       pfl.CreateExample3(plane);
    }
    
}
