/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package facade;

import Model.Sirie;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author 1
 */
@Stateless
public class SirieFacade extends AbstractFacade<Sirie> implements SirieFacadeLocal {
    @PersistenceContext(unitName = "EA-ejbPU2")
    private EntityManager em;
    SessionContext sc;
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SirieFacade() {
        super(Sirie.class);
    }
    
    @Override
    public void createExample1(Sirie sirie)
    {
        em.persist(sirie);
        sc.setRollbackOnly();
    }
    
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @Override
    public void createExample5(Sirie sirie)
    {
        em.persist(sirie);
    }
    
}
