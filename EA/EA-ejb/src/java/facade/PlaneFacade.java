/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package facade;

import Model.Plane;
import javax.ejb.EJBException;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author 1
 */
@Stateless
public class PlaneFacade extends AbstractFacade<Plane> implements PlaneFacadeLocal {
    @PersistenceContext(unitName = "EA-ejbPU")
    private EntityManager em;
    
    SessionContext sctx;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PlaneFacade() {
        super(Plane.class);
    }
    
    public void CreateExample3(Plane plane)
    {
        em.persist(plane);
        em.flush();
        sctx.setRollbackOnly();
        throw new EJBException();
    }
    
    
    @Override
    public void CreanteNewEntituEx2() {
        sctx.setRollbackOnly();
    }
    
    @Override
    @TransactionAttribute(value = TransactionAttributeType.NOT_SUPPORTED)
    public void CreateExample4(Plane plane)
    {
        em.persist(plane);
    }
    
}
