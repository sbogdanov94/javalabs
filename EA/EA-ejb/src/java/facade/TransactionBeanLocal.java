/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package facade;

import Model.Plane;
import Model.Sirie;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author 1
 */
@Local
public interface TransactionBeanLocal { 
    
    public void createNewEntityEX1(Plane plane, Sirie sirie);
    public void createNewEntityEX2(Plane plane, Sirie sirie);
    public void createNewEntityEX3(Plane plane, Sirie sirie);
    public void createNewEntityEX4(Plane plane, Sirie sirie);
    public void createNewEntityEX5(Plane plane, Sirie sirie);
    
}
