/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package facade;

import Model.Plane;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author 1
 */
@Local
public interface PlaneFacadeLocal {

    void create(Plane plane);

    void edit(Plane plane);

    void remove(Plane plane);

    Plane find(Object id);

    List<Plane> findAll();

    List<Plane> findRange(int[] range);

    int count();
    
    public void CreanteNewEntituEx2();
    public void CreateExample3(Plane plane);
    public void CreateExample4(Plane plane);
}
